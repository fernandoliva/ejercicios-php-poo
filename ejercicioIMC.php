<?php
    require_once './ejercicioIMC.class.php';

    $persona = new Persona("Pepe", 50, 1.70);

    //Esto tambien se puede hacer con un switch
    if($persona->imc < 18.5)
    {
        echo $persona->nombre . " tiene un IMC de " . $persona->imc . ". ¡Come un poquito más!";
    } else if($persona->imc >= 18.5 && $persona->imc <= 25)
    {
        echo $persona->nombre . " tiene un IMC de " . $persona->imc . ". ¡¡FELICIDADES!!, tu peso está en la media";
    } else if($persona->imc > 25 && $persona->imc <= 30)
    {
        echo $persona->nombre . " tiene un IMC de " . $persona->imc . ". Sufre de sobrepeso";
    } else if($persona->imc > 30)
    {
        echo $persona->nombre . " tiene un IMC de " . $persona->imc . ". Sufre de obesidad";
    }
