<?php
class User
{
        private string $name;
        private string $email;
        private string $password;
        private int $age;

        //Definir metodo constructor
        public function __construct(string $name, string $email, string $password, int $age)
        {
                $this->name = $name;
                $this->email = $email;
                $this->password = $password;
                $this->age = 0;
        }

        public function age(int $age): void
        {
                $this->age += $age;
        }

        /**
         * Get the value of name
         */
        public function getName()
        {
                return $this->name;
        }

        /**
         * Set the value of name
         *
         * @return  self
         */
        public function setName($name)
        {
                $this->name = $name;

                return $this;
        }

        /**
         * Get the value of email
         */
        public function getEmail()
        {
                return $this->email;
        }

        /**
         * Set the value of email
         *
         * @return  self
         */
        public function setEmail($email)
        {
                $this->email = $email;

                return $this;
        }

        /**
         * Get the value of password
         */
        public function getPassword()
        {
                return $this->password;
        }

        /**
         * Set the value of password
         *
         * @return  self
         */
        public function setPassword($password)
        {
                $this->password = $password;

                return $this;
        }

        /**
         * Get the value of age
         */
        public function getAge()
        {
                return $this->age;
        }

        /**
         * Set the value of age
         *
         * @return  self
         */
        public function setAge($age)
        {
                $this->age = $age;

                return $this;
        }

        public function pintar()
        {
                echo "<ul>";
                echo "<li>Nombre: $this->name</li>";
                echo "<li>Email: $this->email</li>";
                echo "<li>Password: $this->password</li>";
                echo "<li>Age: $this->age</li>";
                echo "</ul>";
        }
}

    // class Film
    // {
    //     public string $title;
    //     public string $description;
    //     public string $img;
    //     public int $release_year;
    //     public string $language;
    //     public int $length;
    //     public int $rating;

    //     public function __construct($title, $description, $img, $release_year, $language, $length, $rating)
    //     {
    //         $this->title = $title;
    //         $this->description = $description;
    //         $this->img = $img;
    //         $this->release_year = $release_year;
    //         $this->language = $language;
    //         $this->length = $length;
    //         $this->rating = $rating;
    //     }
    // }

    // class Series
    // {
    //     public string $title;
    //     public string $description;
    //     public string $img;
    //     public string $language;
    //     public int $release_year;
    //     public int $seasons;
    //     public int $episodes;
    //     public int $rating;

    //     public function __construct($title, $description, $img, $language, $release_year, $seasons, $episodes, $rating){
    //         $this->title = $title;
    //         $this->description = $description;
    //         $this->img = $img;
    //         $this->language = $language;
    //         $this->release_year = $release_year;
    //         $this->seasons = $seasons;
    //         $this->episodes = $episodes;
    //         $this->rating = $rating;
    //     }

    // }
