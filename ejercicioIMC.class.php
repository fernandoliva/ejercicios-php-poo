<?php
    class Persona
    {
        public $nombre;
        public $peso;
        public $altura;
        public $imc;

        public function __construct($nombre, $peso, $altura)
        {
            $this->nombre = $nombre;
            $this->peso = $peso;
            $this->altura = $altura;
            $this->imc = $this->calcularIMC();
        }

        public function calcularIMC()
        {
            return $this->peso / ($this->altura * $this->altura);
        }
    }
